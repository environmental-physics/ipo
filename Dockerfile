FROM jupyter/scipy-notebook:d990a62010ae

RUN conda install --quiet --yes \
    'xarray' \
    'gsw' \ 
    'netCDF4' \
    && \
    conda clean --all -f -y && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

USER root
COPY ipo ipo
RUN fix-permissions /home/$NB_USER
USER $NB_USER

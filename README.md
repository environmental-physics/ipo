# Open in binder

Go to https://mybinder.org/v2/gl/environmental-physics%2Fipo/master.

# Run locally

`docker run -it -p 8888:8888 registry.gitlab.com/environmental-physics/ipo/tutorials:latest`

If you have issues with opening the URL, try `--net host` instead of `-p 8888:8888`. If you want to save your files locally, you have to mount your local folder into the container. Do this by setting `-v ${PWD}/ipo:/home/jovyan/ipo`. Depending on your setup permission errors can occur. In this casea also specify `--user $UID` and add yourself to the group `users` by setting `--group-add users`.
